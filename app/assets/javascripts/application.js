// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require bootstrap-sprockets
//= require jquery_ujs
//= require jquery.turbolinks
//= require turbolinks
//= require_tree .

// おまじない
$(function(){
  // jqueryが発火するように
  $( document ).on('turbolinks:load', function() {
    // グローバルナビの変数宣言
    var nav = $("#global-nav");
    var pos = nav.offset().top;

    // ナビがトップにきたらfixedにする
    $(window).scroll(function () {
      var fix = ($(this).scrollTop() > pos) ? true : false;
      nav.toggleClass("fix-nav", fix);
      $("body").toggleClass("fix-body", fix);
    });
  }); // jqueryが発火するように
}); // おまじないここまで
